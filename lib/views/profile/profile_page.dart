import 'package:apps_voice_commands/controllers/user_controller.dart';
import 'package:apps_voice_commands/views/authentication/login_page.dart';
import 'package:apps_voice_commands/widgets/apps_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final UserController userController = Get.find();

  final ts = TextStyle(
      fontSize: 12.sp, fontWeight: FontWeight.w600, color: AppsTheme.primary);

  final delayed = const Duration(milliseconds: 700);

  Future signout() async {
    await userController.signOut();
    await Future.delayed(delayed).then((value) => Get.off(const LoginPage()));
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Container(
        width: 1.sw,
        height: 1.sh,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [buttonSignout()],
        ),
      );

  Widget buttonSignout() =>
      InkWell(onTap: () => signout(), child: Text('Sign out', style: ts));
}

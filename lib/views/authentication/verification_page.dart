import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class VerificationScreen extends StatefulWidget {
  const VerificationScreen({Key? key}) : super(key: key);

  @override
  _VerificationScreenState createState() => _VerificationScreenState();
}

class _VerificationScreenState extends State<VerificationScreen> {
  @override
  void initState() {
    Timer.periodic(const Duration(seconds: 5),
        (timer) => FirebaseAuth.instance.currentUser?.reload());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return bannerVerifikasi();
  }

  Widget bannerVerifikasi() => Container(
        child: Column(
          children: [
            Icon(Icons.email, size: 30.sp),
            SizedBox(height: 10.h),
            Text(
              'We have sent the verification link in your email',
              style: TextStyle(
                fontSize: 15.sp,
                fontWeight: FontWeight.w700,
                color: Colors.black,
              ),
            )
          ],
        ),
      );
}

import 'package:apps_voice_commands/controllers/user_controller.dart';
import 'package:apps_voice_commands/views/authentication/register_page.dart';
import 'package:apps_voice_commands/widgets/apps_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'widget_logo.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final UserController userController = Get.find();

  final TextEditingController emailTextCtrl = TextEditingController();
  final TextEditingController passwdTextCtrl = TextEditingController();

  final p = EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w);

  final ts = TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w600);

  final styleLogin = TextStyle(
      color: Colors.white, fontSize: 14.sp, fontWeight: FontWeight.w700);

  bool securePassword = true;

  Future loginUser() async {
    final String email = emailTextCtrl.text;
    final String password = passwdTextCtrl.text;

    if (email.isEmpty) {
      Get.snackbar('Login gagal', 'Email tidak boleh kosong');
    } else if (password.isEmpty) {
      Get.snackbar('Login gagal', 'Password tidak boleh kosong');
    } else {
      var response = await userController.signInUser(email, password);

      if (response == 'login user berhasil') {
        Get.snackbar('Login sukses', 'Login user berhasil');
      } else {
        Get.snackbar('Login gagal', response.toString());
      }
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: p,
          width: 1.sw,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              logo(),
              SizedBox(height: 15.h),
              fieldEmail(),
              SizedBox(height: 10.h),
              fieldPasswd(),
              SizedBox(height: 10.h),
              fieldLogin(),
              SizedBox(height: 10.h),
              nav2Register(),
            ],
          ),
        ),
      ),
    );
  }

  Widget logo() => const WidgetLogo();

  Widget fieldEmail() => TextField(
        controller: emailTextCtrl,
        decoration: InputDecoration(
            hintText: 'Input email anda',
            border: const UnderlineInputBorder(),
            hintStyle: ts),
      );

  Widget fieldPasswd() => TextField(
        controller: passwdTextCtrl,
        obscureText: securePassword,
        decoration: InputDecoration(
          hintText: 'Input password anda',
          // border: InputBorder.none,
          border: const UnderlineInputBorder(),
          hintStyle: ts,
          suffixIcon: InkWell(
            onTap: () => setState(() => securePassword = !securePassword),
            child: Icon(
                (securePassword) ? Icons.visibility_off : Icons.visibility),
          ),
        ),
      );

  Widget fieldLogin() => InkWell(
        onTap: () => loginUser(),
        child: Container(
          height: 40.h,
          width: double.infinity,
          decoration: BoxDecoration(
              color: AppsTheme.primary,
              borderRadius: BorderRadius.circular(7.r)),
          child: Center(
            child: Text('Login', style: styleLogin),
          ),
        ),
      );

  Widget nav2Register() => TextButton(
        onPressed: () => Get.to(const RegisterPage()),
        child: Text(
          'Atau register',
          style: TextStyle(
              fontSize: 12.sp,
              fontWeight: FontWeight.w600,
              color: AppsTheme.primary),
        ),
      );
}

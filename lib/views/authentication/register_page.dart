import 'package:apps_voice_commands/controllers/user_controller.dart';
import 'package:apps_voice_commands/widgets/apps_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'widget_logo.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final UserController userController = Get.find();

  final TextEditingController emailTextCtrl = TextEditingController();
  final TextEditingController passwordTextCtrl = TextEditingController();
  final TextEditingController cfmPasswordTextCtrl = TextEditingController();

  final p = EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w);

  final ts = TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w600);

  bool securePwd = true;
  bool secureCfmPwd = true;

  var registerSuccess = false.obs;

  Future register() async {
    final email = emailTextCtrl.text;
    final pwd = passwordTextCtrl.text;
    final cfm = cfmPasswordTextCtrl.text;

    if (pwd != cfm) {
      Get.snackbar('Register', 'Password tidak sama');
    } else if (email.isEmpty) {
      Get.snackbar('Register', 'Email tidak boleh kosong');
    } else if (pwd.isEmpty) {
      Get.snackbar('Register', 'Password tidak boleh kosong');
    } else {
      var response = await userController.createUser(email, pwd);

      if (response ==
          "Register user berhasil, silahkan lakukan verifikasi email") {
        // print('Register user berhasil');
        registerSuccess.value = true;
        Get.snackbar('Keterangan', response);
      } else {
        Get.snackbar('Keterangan', response);
      }
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: 1.sw,
          padding: p,
          color: Colors.white,
          child: Obx(
            () => (registerSuccess.value)
                ? bannerVerifikasi()
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      logo(),
                      SizedBox(height: 15.h),
                      fieldEmail(),
                      SizedBox(height: 10.h),
                      fieldPasswd(),
                      SizedBox(height: 20.h),
                      fieldKonfirmasiPasswrd(),
                      SizedBox(height: 20.h),
                      fieldRegister(),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  Widget logo() => const WidgetLogo();

  Widget fieldEmail() => TextField(
        controller: emailTextCtrl,
        decoration: InputDecoration(
            hintText: 'email',
            border: const UnderlineInputBorder(),
            hintStyle: ts),
      );

  Widget fieldPasswd() => TextField(
        controller: passwordTextCtrl,
        obscureText: securePwd,
        decoration: InputDecoration(
          hintText: 'password',
          border: const UnderlineInputBorder(),
          hintStyle: ts,
          suffixIcon: InkWell(
            onTap: () => setState(() => securePwd = !securePwd),
            child: Icon((securePwd) ? Icons.visibility_off : Icons.visibility),
          ),
        ),
      );

  Widget fieldKonfirmasiPasswrd() => TextField(
        controller: cfmPasswordTextCtrl,
        obscureText: secureCfmPwd,
        decoration: InputDecoration(
          hintText: 'konfirmasi password',
          border: const UnderlineInputBorder(),
          hintStyle: ts,
          suffixIcon: InkWell(
              onTap: () => setState(() => secureCfmPwd = !secureCfmPwd),
              child: Icon(
                  (secureCfmPwd) ? Icons.visibility_off : Icons.visibility)),
        ),
      );

  Widget fieldRegister() => InkWell(
        onTap: () => register(),
        child: Container(
          height: 30.h,
          width: double.infinity,
          decoration: BoxDecoration(
            color: AppsTheme.primary,
            borderRadius: BorderRadius.circular(7.r),
          ),
          child: Center(
            child: Text(
              'Register',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w700),
            ),
          ),
        ),
      );

  Widget bannerVerifikasi() => Container(
        child: Column(
          children: [
            Icon(Icons.email, size: 30.sp),
            SizedBox(height: 10.h),
            Text(
              'We have sent the verification link in your email',
              style: TextStyle(
                fontSize: 15.sp,
                fontWeight: FontWeight.w700,
                color: Colors.black,
              ),
            )
          ],
        ),
      );
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WidgetLogo extends StatelessWidget {
  const WidgetLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => img();

  Widget img() =>
      Image.asset("assets/images/logo.png", width: 120.w, height: 120.h);
}

import 'package:apps_voice_commands/widgets/apps_theme.dart';
import 'package:apps_voice_commands/widgets/audio_record_widget.dart';
import 'package:apps_voice_commands/widgets/process_banner_widget.dart';
import 'package:apps_voice_commands/widgets/profile_bar_widget.dart';
import 'package:apps_voice_commands/widgets/widget_server_options.dart';
import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class Homepage extends StatefulWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 1.sh,
      width: 1.sw,
      child: Stack(
        children: [
          Positioned(top: 0.h, child: main()),
          // Positioned(
          //   top: 100.h,
          //   left: 35.w,
          //   child: WidgetProcessBanner(),
          // )
        ],
      ),
    );
  }

  Widget main() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          background(),
          SizedBox(height: 50.h),
          audioRecord(),
        ],
      );

  Widget background() => Container(
        height: 130.h,
        width: 1.sw,
        padding: EdgeInsets.only(left: 10.w, top: 10.h),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10.r),
            bottomRight: Radius.circular(10.r),
          ),
          color: AppsTheme.primary,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                // const WidgetProfileBar(),
                Container(
                  width: 40.w,
                  height: 20.h,
                ),
                logo(),
                const WidgetServerOptions(),
              ],
            ),
          ],
        ),
      );

  Widget audioRecord() => const AudioRecord();

  Widget logo() => CircleAvatar(
        child:
            Image.asset("assets/images/logo.png", width: 100.w, height: 60.h),
        radius: 50.r,
        backgroundColor: Colors.white,
      );
}

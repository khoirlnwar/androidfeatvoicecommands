import 'dart:async';

import 'controllers/user_controller.dart';
import 'views/authentication/login_page.dart';
import 'views/notification/notif_page.dart';
import 'views/profile/profile_page.dart';
import 'widgets/apps_theme.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import 'views/home/home_page.dart';

import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  final UserController userController = Get.put(UserController());

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: () => GetMaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Apps Control',
        builder: (context, widget) {
          ScreenUtil.setContext(context);
          return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: widget!);
        },
        theme: ThemeData(primarySwatch: Colors.blue, fontFamily: 'Inter'),
        home: const Initial(),
      ),
    );
  }
}

class Initial extends StatefulWidget {
  const Initial({Key? key}) : super(key: key);

  @override
  _InitialState createState() => _InitialState();
}

class _InitialState extends State<Initial> {
  var sendingEmailVerification = false.obs;

  void navigate2Home() => Get.off(const BottomNavbarMain());

  void navigate2Login() => Get.off(const LoginPage());

  // void saySomething() {
  //   print("test");
  //   // nggak perlu return
  // }
  //
  // String saySomething2() {
  //   print("test");
  //   return "test";
  // }

  Timer? timer;

  void process() {
    FirebaseAuth.instance.userChanges().listen((User? user) {
      if (user == null) {
        navigate2Login();
        print("navigate to login");
      } else {
        if (user.emailVerified == true) {
          print("Navigate to home");

          timer?.cancel();

          print(timer?.isActive);
          navigate2Home();
        } else {
          print("sending verification to email");
          sendingEmailVerification.value = true;
          sendEmailVerification(user);

          startTimer();
        }
      }
    });
  }

  // kalau tidak pake firebase, alternatif pakai MQTT
  // MQTT -> Protokol iot
  // python -> ESP32 (Koneksi MQTT) -> alternatif tidak pakai firebase
  Future sendEmailVerification(User user) async {
    await user.sendEmailVerification();
  }

  startTimer() {
    timer = Timer.periodic(const Duration(seconds: 5), (timer) {
      var instance = FirebaseAuth.instance;

      if (instance.currentUser!.emailVerified) {
        timer.cancel();
      } else {
        instance.currentUser?.reload();
      }
    });
  }

  @override
  void initState() {
    startTimer();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    process();

    return Scaffold(
      body: Container(
        height: 1.sh,
        width: 1.sw,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // loading widget
            const CircularProgressIndicator(),
            Obx(() => (sendingEmailVerification.value == true)
                ? bannerVerifikasi()
                : Container())
          ],
        ),
      ),
    );
  }

  Widget bannerVerifikasi() => Container(
        child: Column(
          children: [
            Icon(Icons.email, size: 30.sp),
            SizedBox(height: 10.h),
            Text(
              'We have sent the verification link in your email',
              style: TextStyle(
                fontSize: 15.sp,
                fontWeight: FontWeight.w700,
                color: Colors.black,
              ),
            )
          ],
        ),
      );
}

class BottomNavbarMain extends StatefulWidget {
  const BottomNavbarMain({Key? key}) : super(key: key);

  @override
  _BottomNavbarMainState createState() => _BottomNavbarMainState();
}

class _BottomNavbarMainState extends State<BottomNavbarMain> {
  int currentIndex = 0;

  final List<Widget> children = [
    const Homepage(),
    // const NotifPage(),
    ProfilePage(),
  ];

  final List<BottomNavigationBarItem> list_ = [
    const BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
    // const BottomNavigationBarItem(icon: Icon(Icons.favorite), label: 'Notif'),
    const BottomNavigationBarItem(icon: Icon(Icons.logout), label: 'Logout')
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: children[currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          items: list_,
          currentIndex: currentIndex,
          selectedItemColor: AppsTheme.secondary,
          onTap: onTabTapped,
        ),
      ),
    );
  }

  void onTabTapped(int index) => setState(() => currentIndex = index);
}

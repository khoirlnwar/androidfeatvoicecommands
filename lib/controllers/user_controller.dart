import 'package:apps_voice_commands/utils/user_netutils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

class UserController extends GetxController {
  final UserNetutils netutils = UserNetutils();

  Future createUser(String email, String password) async {
    var response = await netutils.createUser(email, password);
    var return_;

    if (response["kode"] == "1") {
      return_ = 'Register user berhasil, silahkan lakukan verifikasi email';
    } else if (response["kode"] == "-1") {
      return_ = response["detail"];
    }

    // if (response == 'weak-password') {
    //   return_ = 'Password lemah';
    // } else if (response == 'email-already-in-use') {
    //   return_ = 'Email sudah digunakan';
    // } else {
    //   UserCredential userCredential = response;
    //   return_ = 'Register user berhasil';
    // }

    return return_;
  }

  Future signInUser(String email, String password) async {
    var response = await netutils.signInUser(email, password);
    var return_;

    if (response == 'user-not-found') {
      return_ = response;
    } else if (response == 'wrong-password') {
      return_ = response;
    } else {
      UserCredential userCredential = response;
      return_ = 'login user berhasil';
    }

    return return_;
  }

  Future signOut() async {
    var resp = await netutils.signOut();
  }
}

import 'package:apps_voice_commands/utils/server_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'apps_theme.dart';

class WidgetServerOptions extends StatefulWidget {
  const WidgetServerOptions({Key? key}) : super(key: key);

  @override
  State<WidgetServerOptions> createState() => _WidgetServerOptionsState();
}

class _WidgetServerOptionsState extends State<WidgetServerOptions> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController serverController = TextEditingController();

  loadServer() async {
    if (ServerOptions.serverUtama != "") {
      serverController.text = ServerOptions.serverUtama;
    } else {
      await ServerOptions.loadLastServer();
      serverController.text = ServerOptions.serverUtama;
    }
  }

  @override
  void initState() {
    loadServer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 10.w), child: settings());
  }

  Widget settings() => GestureDetector(
        onTap: () {
          // showBottomSheet(
          //   context: context,
          //   builder: (context) => Container(
          //     padding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 5.w),
          //     decoration: BoxDecoration(
          //         color: Colors.white,
          //         border: Border.all(color: Colors.black12),
          //         borderRadius:
          //             BorderRadius.only(topLeft: Radius.circular(8.r))),
          //     child: Wrap(
          //       children: [
          //         SizedBox(height: 10.h),
          //         TextFormField(controller: serverController),
          //         SizedBox(height: 10.h),
          //         button(),
          //       ],
          //     ),
          //   ),
          // );
          showModalBottomSheet(
            context: context,
            builder: (context) => Container(
              height: 400.h,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 18.w),
                child: inputServer(),
              ),
            ),
            isScrollControlled: true,
          );
        },
        child: Icon(Icons.settings, size: 27.sp, color: Colors.white),
      );

  Widget inputServer() => Column(
        // mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextFormField(controller: serverController),
          SizedBox(height: 180.h),
          button(),
          SizedBox(height: 2.h),
        ],
      );

  Widget button() => InkWell(
        onTap: () async {
          if (serverController.text.isNotEmpty) {
            await ServerOptions.setServer(serverController.text);
            await ServerOptions.loadLastServer();

            Navigator.pop(context);
          }
        },
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5.h, horizontal: 5.w),
          height: 40.h,
          width: double.infinity,
          decoration: BoxDecoration(
              color: AppsTheme.primary,
              borderRadius: BorderRadius.circular(7.r)),
          child: Center(
            child: Text(
              'Simpan alamat server',
              style: TextStyle(
                color: Colors.white,
                fontSize: 14.sp,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
      );
}

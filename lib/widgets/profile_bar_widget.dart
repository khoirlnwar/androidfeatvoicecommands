import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'apps_theme.dart';

class WidgetProfileBar extends StatefulWidget {
  const WidgetProfileBar({Key? key}) : super(key: key);

  @override
  _WidgetProfileBarState createState() => _WidgetProfileBarState();
}

class _WidgetProfileBarState extends State<WidgetProfileBar> {
  @override
  Widget build(BuildContext context) => Container(
      // height: 45.h,
      // width: 150.w,
      // color: Colors.transparent,
      // child: Row(
      //   mainAxisAlignment: MainAxisAlignment.start,
      //   crossAxisAlignment: CrossAxisAlignment.center,
      //   children: [photo(), SizedBox(width: 10.w), nama()],
      // ),
      );

  Widget photo() => CircleAvatar(
      radius: 18.r,
      backgroundColor: Colors.white,
      child: Icon(Icons.person, color: AppsTheme.secondary, size: 14.sp));

  Widget nama() => Text("Halo user", style: AppsTheme.tsusername);
}

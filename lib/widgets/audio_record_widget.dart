import 'dart:io' as io;
import 'dart:io';

import 'package:apps_voice_commands/utils/server_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder2/flutter_audio_recorder2.dart';
import 'package:just_audio/just_audio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:record/record.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:file/file.dart';
import 'package:file/local.dart';

import 'apps_theme.dart';

class AudioRecord extends StatefulWidget {
  const AudioRecord({Key? key}) : super(key: key);

  @override
  _AudioRecordState createState() => _AudioRecordState();
}

class _AudioRecordState extends State<AudioRecord> {
  final LocalFileSystem localFileSystem = LocalFileSystem();

  TextEditingController serverTextCtrl = new TextEditingController();

  late AudioPlayer player;
  var records = Record();

  String rpath = "";
  var isRecording = false.obs;

  bool? hasPermission;
  FlutterAudioRecorder2? audioRecorder;
  Recording? current_;

  File? resultFile;

  final ts = TextStyle(
    fontSize: 13.sp,
    fontWeight: FontWeight.w700,
    color: Colors.blue,
  );

  final styleLogin = TextStyle(
      color: Colors.white, fontSize: 14.sp, fontWeight: FontWeight.w700);

  init() async {
    bool result = await records.hasPermission();
    player = AudioPlayer();
  }

  // init:
  // 1. minta izin / permission
  initialize() async {
    hasPermission = await FlutterAudioRecorder2.hasPermissions;
    // audioRecorder = FlutterAudioRecorder2("file_audio_path.wav");
    //
    // await audioRecorder!.initialized;

    if (hasPermission! == true) {
      String customPath = "/flutter_audio_recorder_";

      io.Directory appDocDirectory = (await getExternalStorageDirectory())!;

      customPath = appDocDirectory.path +
          customPath +
          DateTime.now().millisecondsSinceEpoch.toString();
      audioRecorder =
          FlutterAudioRecorder2(customPath, audioFormat: AudioFormat.WAV);

      // nama folder nya unik tergantung tanggal/waktu
      // /flutter_audio_recorder_260320221423xxxx

      // initial library audioRecored
      await audioRecorder!.initialized;

      // cek status audio recorder
      var current = await audioRecorder!.current(channel: 0);
      print("current status: $current");

      setState(() {
        current_ = current;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    init();
    initialize();
  }

  @override
  void dispose() {
    player.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 0.6.sh,
      width: 1.sw,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              btn(),
              Container(height: 4.h),
              iplay(),
              Container(height: 10.h),
            ],
          ),
          button(),
        ],
      ),
    );
  }

  // Recording: Start
  Future rstart() async {
    Map<Permission, PermissionStatus> permissions = await [
      Permission.storage,
      Permission.microphone,
    ].request();

    bool permissionsGranted = permissions[Permission.storage]!.isGranted &&
        permissions[Permission.microphone]!.isGranted;

    if (permissionsGranted) {
      // await records.start(encoder: AudioEncoder.AAC_HE);
      isRecording.value = true;

      // mulai rekam audio:
      await audioRecorder!.start();
      var recording = await audioRecorder!.current(channel: 0);

      // cek status perekaman audio
      // status: recording
      print("status: ");
      print(recording!.status);

      setState(() {
        current_ = recording;
      });
    } else {
      // ignore: avoid_print
      print("Permission not granted");
    }
  }

  // Recording: stop
  Future rstop() async {
    // String? path = await records.stop();
    isRecording.value = false;

    // mulai simpan audio hasil recording
    // result: hasil dari recording
    var result = await audioRecorder!.stop();

    // transformasi variable result ke data File
    File file = localFileSystem.file(result!.path);

    resultFile = file;
    print("path: ${result.path}");

    setState(() {
      current_ = result;
    });

    // player.setFilePath(result.path!);

    // if (path != null) {
    //   print("Path configured: ");
    //   print(path);
    //
    // }
  }

  void onPlayAudio() async {
    AudioPlayer audioPlayer = AudioPlayer();
    audioPlayer.setFilePath(current_!.path!);

    await audioPlayer.play();
  }

  Widget iplay() => InkWell(
        onTap: () => (!isRecording.value) ? onPlayAudio() : null,
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5.h, horizontal: 5.w),
          height: 40.h,
          width: double.infinity,
          decoration: BoxDecoration(
              color: AppsTheme.primary,
              borderRadius: BorderRadius.circular(7.r)),
          child: Center(
            child: Text("Play records", style: styleLogin),
          ),
        ),
      );

  Widget btn() => InkWell(
        onTap: () => (isRecording.value) ? rstop() : rstart(),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5.h, horizontal: 5.w),
          height: 40.h,
          width: double.infinity,
          decoration: BoxDecoration(
              color: AppsTheme.primary,
              borderRadius: BorderRadius.circular(7.r)),
          child: Center(
            child: Obx(
              () => Text(
                  (isRecording.value) ? 'Stop recording' : 'Start recording',
                  style: styleLogin),
            ),
          ),
        ),
      );

  Widget button() => InkWell(
        onTap: () => (resultFile == null) ? null : onButtonSubmit(),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5.h, horizontal: 5.w),
          height: 40.h,
          width: double.infinity,
          decoration: BoxDecoration(
              color: AppsTheme.primary,
              borderRadius: BorderRadius.circular(7.r)),
          child: Center(
            child: Text(
              (resultFile == null) ? 'Audio belum direkam' : 'Kirim audio',
              style: styleLogin,
            ),
          ),
        ),
      );

  // Aksi ketika tekan button kirim audio
  Future onButtonSubmit() async {
    ServerUtils serverUtils = new ServerUtils();

    if (resultFile != null) {
      Get.snackbar('Mengirim', 'Audio dikirim ke ${ServerOptions.serverUtama}');
      var response = await serverUtils.uploadFile(resultFile!);
    }
  }
}

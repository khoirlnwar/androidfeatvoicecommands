import 'package:firebase_auth/firebase_auth.dart';

class UserNetutils {
  Future createUser(String email, String password) async {
    // ignore: prefer_typing_uninitialized_variables
    var response;

    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      // response = userCredential;
      response = {"kode": "1", "detail": userCredential};
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        response = {"kode": "-1", "detail": e.code};
      } else if (e.code == 'email-already-in-use') {
        response = {"kode": "-1", "detail": e.code};
      } else {
        response = {"kode": "-1", "detail": e.message};
      }
    }

    return response;
  }

  Future signInUser(String email, String password) async {
    // ignore: prefer_typing_uninitialized_variables
    var response;

    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      response = userCredential;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        // ignore: avoid_print
        print('No user found for that email.');
        response = 'user-not-found';
      } else if (e.code == 'wrong-password') {
        // ignore: avoid_print
        print('Wrong password provided for that user.');
        response = 'wrong-password';
      }
    }

    return response;
  }

  Future signOut() async {
    var response = await FirebaseAuth.instance.signOut();
  }
}

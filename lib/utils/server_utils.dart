import 'dart:io';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ServerOptions {
  static const String keyPrefs = "SERVER";
  static String serverUtama = "192.168.1.8";

  // get from shared prefs
  static loadLastServer() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var data = sharedPreferences.getString(keyPrefs);

    serverUtama = data!;
  }

  // set to shared prefs
  static setServer(String addrs) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString(keyPrefs, addrs);

    serverUtama = addrs;
  }
}

// class ini mendefinisikan alamat SERVER dan endpoint yang digunakan
class ServerUtils {
  // Future: khusus untuk fungsi asinkronus
  Future uploadFile(File file) async {
    const String endpoint = "/api/audio/text";

    String filename = file.path.split('/').last;

    // sertakan file audio.wav ke parameter sebelum dikirim ke server
    FormData data = FormData.fromMap({
      "audio": await MultipartFile.fromFile(file.path, filename: filename),
    });

    // Dio: Library HTTP Client
    Dio dio = new Dio();

    print(ServerOptions.serverUtama);
    dio.options.baseUrl = "http://${ServerOptions.serverUtama}:5000";
    var response = await dio.post(endpoint, data: data);
  }
}
